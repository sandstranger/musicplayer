package data;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.Song;
import utils.Constants;
import utils.loaders.AssetsLoader;
import utils.log.LogPrinter;

public class SongsFetcher {

    public static List<Song> fetchSongs() {
        List<Song> songsList = new ArrayList<>();
        try {
            String jsonContent = AssetsLoader.readTextFromAssetsFile(Constants.JSON_SONGS_FILE_NAME);
            com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
            JsonElement element = jsonParser.parse(jsonContent);
            JsonArray jsonArray = element.getAsJsonArray();
            Gson gson = new Gson();
            songsList = new ArrayList<Song>(Arrays.asList(gson.fromJson(jsonArray, Song[].class)));
        } catch (Exception e) {
            LogPrinter.printExceptionToLogcat(e);
        }
        return songsList;
    }
}
