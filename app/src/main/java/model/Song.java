package model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.Getter;

@Data
public class Song {

    @SerializedName("id")
    @Getter private int songId;
    @SerializedName("name")
    @Getter private String songName;
    @SerializedName("image")
    @Getter private String songPictureLink;
    @SerializedName("audio")
    @Getter private String songLink;

    public Song (){
        
    }
}
