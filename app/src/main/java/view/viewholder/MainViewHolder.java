package view.viewholder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.musicplayer.player.R;

import lombok.Data;
import lombok.Getter;
import main.ApplicationContextProvider;
import model.Song;
import utils.Constants;
import utils.loaders.PictureLoader;
import view.activity.ISongListener;
import view.activity.MainActivity;
import view.adapter.SongsAdapter;

@Data
public class MainViewHolder {
    private SongsAdapter.ViewHolder viewHolderItem;
    @Getter
    private ProgressDialog progressDialog;
    @Getter
    private PlayerWidgetViewHolder playerWidget;
    private Activity activity;
    private ISongListener listener;
    @Getter
    private SongsAdapter songsAdapter;
    @Getter
    private RecyclerView recyclerView;
    @Getter
    private ImageButton finishAppBtn;

    public MainViewHolder(MainActivity activity) {
        progressDialog = new ProgressDialog(activity);
        this.activity = activity;
        listener = activity;
        playerWidget = new PlayerWidgetViewHolder(activity);
        songsAdapter = new SongsAdapter(activity);
        recyclerView = (RecyclerView) activity.findViewById(R.id.songsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(songsAdapter);
        finishAppBtn = (ImageButton) activity.findViewById(R.id.finishAppBtn);
    }

    public void bindView() {
        finishAppBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        playerWidget.bindView();
    }

    public void showPlayerWidget(Song song) {
        viewHolderItem = songsAdapter.getAdapterViewItem(song.getSongId());
        int seekBarMaxProgress = listener.getMusicPlayer().getTrackDuration() / Constants.SEEKBAR_OFFSET;
        if (viewHolderItem != null) {
            viewHolderItem.resetSeekBar();
            viewHolderItem.setSeekBarMaxProgress(seekBarMaxProgress);
        }
        PictureLoader.loadPictureToView(song.getSongPictureLink(), playerWidget.getSongPictureView());
        playerWidget.getSongNameTextView().setText(ApplicationContextProvider.getContext()
                .getString(R.string.txt_now_playing_preset) + ": " + song.getSongName());
        playerWidget.getSongLengthSeekbar().setProgress(0);
        playerWidget.getSongLengthSeekbar().setMax(seekBarMaxProgress);
        playerWidget.showPlayerWidget(true);
    }

    public void updatePlayerWidgetSeekBarProgress(int progress) {
        playerWidget.getSongLengthSeekbar().setProgress(progress);
        if (viewHolderItem != null) {
            viewHolderItem.setSeekBarProgress(progress);
        }
    }

    public void showProgressDialog() {
        progressDialog = ProgressDialog.show(
                activity, "", activity.getString(R.string.txt_audio_buffering), true);
    }

    public void hideProgressDialog() {
        if (progressDialog!=null) {
            progressDialog.dismiss();
        }
    }
}