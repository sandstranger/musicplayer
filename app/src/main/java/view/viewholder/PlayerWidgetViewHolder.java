package view.viewholder;

import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.musicplayer.player.R;

import lombok.Data;
import lombok.Getter;
import player.MusicPlayer;
import utils.Constants;
import view.activity.ISongListener;

@Data
public class PlayerWidgetViewHolder {
    @Getter
    private ImageView songPictureView;
    @Getter
    private LinearLayout songPictureViewRootLayout;
    private ISongListener listener;
    @Getter
    private LinearLayout rootLayout;
    @Getter
    private ImageButton stopPlayingBtn;
    @Getter
    private TextView songNameTextView;
    @Getter
    private SeekBar songLengthSeekbar;

    public PlayerWidgetViewHolder(Activity activity) {
        this.listener = (ISongListener) activity;
        songPictureViewRootLayout = (LinearLayout) activity.findViewById(R.id.songPictureViewRootLayout);
        songPictureView = (ImageView) activity.findViewById(R.id.songPictureView);
        rootLayout = (LinearLayout) activity.findViewById(R.id.playerRootLayout);
        stopPlayingBtn = (ImageButton) activity.findViewById(R.id.stopPlayingBtn);
        songNameTextView = (TextView) activity.findViewById(R.id.songNameTextView);
        songLengthSeekbar = (SeekBar) activity.findViewById(R.id.songLengthSeekbar);
    }

    public void bindView() {
        stopPlayingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPlayerWidget(false);
                listener.stopPlaying();
            }
        });
        songLengthSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                MusicPlayer player = listener.getMusicPlayer();
                if (player.isPlaying() && fromUser) {
                    player.seekTo(progress * Constants.SEEKBAR_OFFSET);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void showPlayerWidget(boolean show) {
        int visibilityFlag = show ? View.VISIBLE : View.GONE;
        rootLayout.setVisibility(visibilityFlag);
        songPictureViewRootLayout.setVisibility(visibilityFlag);
    }
}