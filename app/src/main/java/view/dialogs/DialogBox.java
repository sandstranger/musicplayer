package view.dialogs;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.musicplayer.player.R;

public class DialogBox {
    public static void showDialogBox(Context context, int messageId) {
        new MaterialDialog.Builder(context)
                .content(messageId)
                .positiveText(R.string.okbtn_text)
                .cancelable(true)
                .show();
    }
}
