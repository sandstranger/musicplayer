package view.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.musicplayer.player.R;

import model.Song;
import player.IDownloadingSongListener;
import player.MusicPlayer;
import utils.Constants;
import view.dialogs.DialogBox;
import view.viewholder.MainViewHolder;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, IDownloadingSongListener, ISongListener {
    private MainViewHolder viewHolder;
    private MusicPlayer musicPlayer = new MusicPlayer();
    private boolean isPlayerPlayingOnPause = false;
    private Handler handler = new Handler();
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        musicPlayer.setOnCompletionListener(this);
        musicPlayer.setOnPreparedListener(this);
        musicPlayer.setOnErrorListener(this);
    }

    private void bindView() {
        setContentView(R.layout.activity_main);
        viewHolder = new MainViewHolder(this);
        viewHolder.bindView();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if (musicPlayer.isInit()) {
            startPlaying(musicPlayer.getPlaylist().getNextSong(musicPlayer.getCurrentSong()));
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        viewHolder.hideProgressDialog();
        startPlaying();
        viewHolder.showPlayerWidget(musicPlayer.getCurrentSong());
    }

    @Override
    public void startPlaying(Song song) {
        if (musicPlayer.getCurrentSong().getSongId() != song.getSongId()) {
            viewHolder.getSongsAdapter().updateStartPlayingBtnVisibility(musicPlayer.getCurrentSong().getSongId(), true);
            viewHolder.getSongsAdapter().updateStartPlayingBtnVisibility(song.getSongId(), false);
            musicPlayer.streamSong(song);
            viewHolder.showProgressDialog();
        } else {
            startPlaying();
        }
    }

    @Override
    public void pausePlaying() {
        if (runnable!=null) {
            handler.removeCallbacks(runnable);
        }
        musicPlayer.pause();
    }

    private void startPlaying (){
        musicPlayer.play();
        startUpdateSeekBarProgress();
    }

    @Override
    public void stopPlaying() {
        if (runnable!=null) {
            handler.removeCallbacks(runnable);
        }
        musicPlayer.stop();
        viewHolder.getSongsAdapter().onMusicPlayingStopped();
    }

    @Override
    public MusicPlayer getMusicPlayer() {
        return musicPlayer;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPlayerPlayingOnPause = musicPlayer.isPlaying();
        if (isPlayerPlayingOnPause) {
            pausePlaying();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (musicPlayer.isPaused() && isPlayerPlayingOnPause) {
            startPlaying();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        musicPlayer.releasePlayer();
    }


    private void startUpdateSeekBarProgress() {
        runnable = new Runnable() {
            @Override
            public void run() {
                if (musicPlayer.isPlaying()) {
                    int playerPosition = musicPlayer.getCurrentTrackPosition() / Constants.SEEKBAR_OFFSET;
                    viewHolder.updatePlayerWidgetSeekBarProgress(playerPosition);
                }
                handler.postDelayed(runnable, Constants.SEEKBAR_OFFSET);
            }
        };
        handler.postDelayed(runnable, Constants.SEEKBAR_OFFSET);
    }

    @Override
    public void onDownloadSongFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewHolder.hideProgressDialog();
                stopPlaying();
                viewHolder.getPlayerWidget().showPlayerWidget(false);
                DialogBox.showDialogBox(MainActivity.this,R.string.txt_song_downloading_failed);
            }
        });
    }
}
