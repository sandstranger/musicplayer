package view.activity;

import model.Song;
import player.MusicPlayer;

public interface ISongListener {
    void startPlaying(Song song);
    void pausePlaying();
    void stopPlaying ();
    MusicPlayer getMusicPlayer ();
}
