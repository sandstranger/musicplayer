package view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.musicplayer.player.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import main.ApplicationContextProvider;
import model.Song;
import player.MusicPlayer;
import utils.Constants;
import utils.loaders.PictureLoader;
import view.activity.ISongListener;

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {
    private List<Song> songsList = new ArrayList<>();
    private Map<Integer, ViewHolder> viewHolderMap = new TreeMap<>();
    private ISongListener songListener;

    public SongsAdapter(ISongListener songListener) {
        setHasStableIds(true);
        this.songListener = songListener;
        songsList = songListener.getMusicPlayer().getPlaylist().getSongsList();
    }

    public void updateStartPlayingBtnVisibility(int songPosition, boolean showPlayBtn) {
        ViewHolder viewHolder = viewHolderMap.get(songPosition);
        if (viewHolder != null) {
            if (showPlayBtn) {
                viewHolder.showStartPlayingSongBtn();
            } else {
                viewHolder.showPauseSongBtn();
            }
        }
    }

    public ViewHolder getAdapterViewItem(int position) {
        return viewHolderMap.get(position);
    }

    public void onMusicPlayingStopped() {
        for (Song song : songsList) {
            updateStartPlayingBtnVisibility(song.getSongId(), true);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.song_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Song dataModel = songsList.get(position);
        holder.fillView(dataModel, songListener);
        viewHolderMap.put(dataModel.getSongId(), holder);
    }

    //This methods overrides from superclass
    @Override
    public int getItemCount() {
        return songsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private Song dataModel;
        private ImageView songPictureImageView;
        private SeekBar songLengthSeekBar;
        private TextView songNameTextView;
        private ImageButton startPlayingSongBtn;
        private ImageButton pauseSongBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            songLengthSeekBar = (SeekBar) itemView.findViewById(R.id.songSeekbar);
            songPictureImageView = (ImageView) itemView.findViewById(R.id.songPictureImageView);
            songNameTextView = (TextView) itemView.findViewById(R.id.songNameTextView);
            startPlayingSongBtn = (ImageButton) itemView.findViewById(R.id.startPlayingSongBtn);
            pauseSongBtn = (ImageButton) itemView.findViewById(R.id.pauseSongBtn);
        }

        private void fillView(final Song dataModel, final ISongListener songListener) {
            this.dataModel = dataModel;
            songNameTextView.setText(ApplicationContextProvider.getContext().getString(R.string.txt_preset) + " : " + dataModel.getSongName());
            PictureLoader.loadPictureToView(dataModel.getSongPictureLink(), songPictureImageView);

            pauseSongBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songListener.pausePlaying();
                    showStartPlayingSongBtn();
                }
            });

            startPlayingSongBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    songListener.startPlaying(dataModel);
                    showPauseSongBtn();
                }
            });

            songLengthSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    MusicPlayer player = songListener.getMusicPlayer();
                    if (player.isPlaying() && player.getCurrentSong().getSongId() == ViewHolder.this.dataModel.getSongId() && fromUser) {
                        player.seekTo(progress * Constants.SEEKBAR_OFFSET);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }

        public void showStartPlayingSongBtn() {
            pauseSongBtn.setVisibility(View.GONE);
            startPlayingSongBtn.setVisibility(View.VISIBLE);
        }

        public void showPauseSongBtn() {
            pauseSongBtn.setVisibility(View.VISIBLE);
            startPlayingSongBtn.setVisibility(View.GONE);
        }

        public void resetSeekBar() {
            setSeekBarMaxProgress(0);
        }

        public void setSeekBarProgress (int progress){
            songLengthSeekBar.setProgress(progress);
        }

        public void setSeekBarMaxProgress(int maxProgress) {
            songLengthSeekBar.setMax(maxProgress);
        }
    }
}
