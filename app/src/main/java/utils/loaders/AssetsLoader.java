package utils.loaders;

import android.content.res.AssetManager;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

import main.ApplicationContextProvider;
import utils.log.LogPrinter;

public class AssetsLoader {

    public static String readTextFromAssetsFile (String fileName){
        String fileContent = "";
        try {
            AssetManager assetManager = ApplicationContextProvider.getContext().getAssets();
            InputStream inputStream = assetManager.open(fileName);
            fileContent = IOUtils.toString(inputStream);
            inputStream.close();
        } catch (Exception e) {
            LogPrinter.printExceptionToLogcat(e);
        }
        return fileContent;
    }

}
