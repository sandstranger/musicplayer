package utils.loaders;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PictureLoader {
    public static void loadPictureToView(String imageLink, ImageView targetPictureView) {
        Picasso picasso = Picasso.get();
        picasso.load(imageLink).into(targetPictureView);
    }
}
