package player;

public interface IDownloadingSongListener {
    void onDownloadSongFailed();
}
