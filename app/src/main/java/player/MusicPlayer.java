package player;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;

import java.io.IOException;

import lombok.Data;
import lombok.Getter;
import model.Song;
import utils.log.LogPrinter;

@Data
public class MusicPlayer {
    private static final int SONG_DOWNLOADING_TIMEOUT = 10000;
    private IDownloadingSongListener downloadingSongListener;
    @Getter
    private Playlist playlist = new Playlist();
    private boolean isInit = false;
    @Getter
    private Song currentSong = new Song();
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private Handler handler = new Handler();
    private Runnable runnable;
    private PlayerAsyncDownloadingSongTask playerAsyncDownloadingSongTask;

    public MusicPlayer() {
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public boolean isInit() {
        return isInit;
    }

    public void streamSong(Song song) {
        try {
            cancelDownloadingSong();
            isInit = true;
            this.currentSong = song;
            mediaPlayer.reset();
            mediaPlayer.setDataSource(song.getSongLink());
            playerAsyncDownloadingSongTask = new PlayerAsyncDownloadingSongTask();
            playerAsyncDownloadingSongTask.execute();
            setDownloadingSongTimeout();
        } catch (IOException e) {
            LogPrinter.printExceptionToLogcat(e);
        }
    }

    private boolean cancelDownloadingSong() {
        if (playerAsyncDownloadingSongTask != null && playerAsyncDownloadingSongTask.getStatus() == AsyncTask.Status.RUNNING) {
            playerAsyncDownloadingSongTask.cancel(true);
            return true;
        }
        return false;
    }

    private void setDownloadingSongTimeout() {
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
        runnable = new Runnable() {
            @Override
            public void run() {
                if (cancelDownloadingSong() && downloadingSongListener != null) {
                    downloadingSongListener.onDownloadSongFailed();
                }
            }
        };
        handler.postDelayed(runnable, SONG_DOWNLOADING_TIMEOUT);
    }

    public void play() {
        mediaPlayer.start();
    }

    public void releasePlayer() {
        mediaPlayer.release();
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void pause() {
        mediaPlayer.pause();
    }

    public void stop() {
        currentSong = new Song();
        isInit = false;
        mediaPlayer.stop();
    }

    public void seekTo(int position) {
        if (isPlaying()) {
            mediaPlayer.seekTo(position);
        }
    }

    public int getTrackDuration() {
        return isPlaying() ? mediaPlayer.getDuration() : 0;
    }

    public boolean isPaused() {
        return !isPlaying() && mediaPlayer.getCurrentPosition() > 0;
    }

    public int getCurrentTrackPosition() {
        return isPlaying() ? mediaPlayer.getCurrentPosition() : 0;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener listener) {
        if (listener != null) {
            mediaPlayer.setOnPreparedListener(listener);
        }
    }

    public void setOnErrorListener(IDownloadingSongListener listener) {
        if (listener != null) {
            downloadingSongListener = listener;
        }
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        if (listener != null) {
            mediaPlayer.setOnCompletionListener(listener);
        }
    }

    private class PlayerAsyncDownloadingSongTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                LogPrinter.printExceptionToLogcat(e);
            }
            return null;
        }
    }

}
