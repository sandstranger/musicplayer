package player;

import java.util.ArrayList;
import java.util.List;

import data.SongsFetcher;
import lombok.Data;
import lombok.Getter;
import model.Song;

@Data
public class Playlist {
    @Getter
    private List<Song> songsList = new ArrayList<>();

    public Playlist() {
        songsList = SongsFetcher.fetchSongs();
    }

    public Song getNextSong(Song previousSong) {
        int previousSongPosition = previousSong != null ? songsList.indexOf(previousSong) : 0;
        int nextSongPosition = previousSongPosition >= 0 && previousSongPosition + 1 < songsList.size() ? previousSongPosition + 1 : 0;
        return songsList.get(nextSongPosition);
    }
}
